var gulp = require('gulp');
var concat = require('gulp-concat');
var htmlmin = require('gulp-htmlmin');
var less = require('gulp-less');
var LessPluginCleanCSS = require('less-plugin-clean-css'),
    cleancss = new LessPluginCleanCSS({ advanced: true });
var backendPath = [];
var paths = {
  publicRoot: './public/',
  publicScripts:  './public/scripts/',
  publicStyles:  './public/styles/',
  publicTemplates: './public/templates/',
  devRoot: './src/Frontend/',
  devTemplates: './src/Frontend/template/'
};

gulp.task('default', ['build', 'watch']);

gulp.task('build', ['buildFrontend']);

gulp.task('buildFrontend', [
  'buildScripts',
  'buildStyles',
  'buildTemplates'
]);

gulp.task('watch', () => {
  gulp.watch([paths.devRoot+'/*/**'], ['build']);
});

gulp.task('buildTemplates', () => {
  gulp.src(paths.devTemplates+'Views/*')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest(paths.publicRoot+"templates/"));
});

gulp.task('buildScripts', () => {
  return gulp.src([
    './bower_components/angular/angular.min.js',
    './bower_components/angular-route/angular-route.min.js',
// @todo implement remember me 
//    './bower_components/angular-cookie/angular-cookie.min.js',
    paths.devRoot+'app.js',
    paths.devRoot+'Services/*',
    paths.devRoot+'Controllers/*'
  ])
    .pipe(concat('all.js'))
    .pipe(gulp.dest(paths.publicScripts));
});

gulp.task('buildStyles', function () {
  return gulp.src(paths.devTemplates+'/build.less')
    .pipe(less({
      plugins: [cleancss]
    }))
    .pipe(concat('all.css'))
    .pipe(gulp.dest(paths.publicStyles));
});
