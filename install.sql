CREATE DATABASE `bog` CHARACTER SET utf8 COLLATE utf8_general_ci;

USE bog;

CREATE TABLE Users (
  user_id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(80) NOT NULL,
  hashedPassword CHAR(120) NOT NULL,
  PRIMARY KEY (user_id),
  UNIQUE INDEX (email)
) ENGINE=INNODB;

INSERT INTO Users (`email`, `hashedPassword`)
VALUES ('root@localhost.com', '$2y$10$QEW6SLfs5t.Ej4FoIv.5EeNU/Lx2prLXDMD6WvaME/QTWmgFU4p2a');
