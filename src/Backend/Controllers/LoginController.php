<?php
namespace Skansing\Bog\Backend\Controllers;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Skansing\Bog\Backend\Auth\AuthService;
use \Skansing\Bog\Backend\User\UserService;
use \Skansing\Bog\Backend\Response\JsonResponse;
use \Skansing\Bog\Backend\XSRF\TokenService;

class LoginController
{
  private $request;
  private $response;
  private $tokenService;
  private $authService;
  private $userService;
  private $jsonResponse;

  public function __construct(
    Request $request,
    Response $response,
    TokenService $tokenService,
    AuthService $authService,
    UserService $userService,
    JsonResponse $jsonResponse
  ){
    $this->request = $request;
    $this->response = $response;
    $this->tokenService = $tokenService;
    $this->authService = $authService;
    $this->userService = $userService;
    $this->jsonResponse = $jsonResponse;
  }

  public function index()
  {
    if($this->tokenService->verifyAngularXsrfHeader() === false)
    {
      $this->jsonResponse->set(
        'XSRF Token Failure'
      );

      return $this->jsonResponse->build();
    }
    // @todo extract method n validate content before unpacking
    $data = json_decode($this->request->getContent(), true);
    $authenticated = $this->authService->loginWithEmail(
      $data['email'],
      $data['password']
    );
    if($authenticated === false)
    {
      $this->jsonResponse->set(
        JsonResponse::FAILURE
      );
    }
    else {
      $user = $this->userService->getByEmail($data['email']);
      $this->jsonResponse->set(
        JsonResponse::SUCCESS,
        $user
      );
    }

    return $this->jsonResponse->build();
  }
}
