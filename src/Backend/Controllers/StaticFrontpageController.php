<?php
namespace Skansing\Bog\Backend\Controllers;

class StaticFrontpageController
{

  public function index()
  {
    return file_get_contents(
      __DIR__.'/../../Frontend/template/template.html'
    );
  }
}
