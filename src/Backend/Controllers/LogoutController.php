<?php
namespace Skansing\Bog\Backend\Controllers;

use \Skansing\Bog\Backend\Auth\AuthService;
use \Skansing\Bog\Backend\Response\JsonResponse;
use \Skansing\Bog\Backend\XSRF\TokenService;

class LogoutController
{
  private $tokenService;
  private $authService;
  private $userService;
  private $jsonResponse;

  public function __construct(
    TokenService $tokenService,
    AuthService $authService,
    JsonResponse $jsonResponse
  ){
    $this->tokenService = $tokenService;
    $this->authService = $authService;
    $this->jsonResponse = $jsonResponse;
  }

  public function index()
  {
    if($this->tokenService->verifyAngularXsrfHeader() === false)
    {
      if($authenticated !== true)
      {
        $this->jsonResponse->set(
          'XSRF Token Failure'
        );
      }

      return $this->jsonResponse->build();
    }
    $this->authService->logout();
    $this->jsonResponse->set(
      JsonResponse::SUCCESS
    );

    return $this->jsonResponse->build();
  }
}
