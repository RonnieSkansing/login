<?php
namespace Skansing\Bog\Backend\XSRF;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\Cookie;
use \Symfony\Component\HttpFoundation\Session\Session;

class TokenService
{
  const TOKEN_COOKIE_NAME = 'XSRF-TOKEN';
  const TOKEN_HEADER_NAME = 'X-XSRF-TOKEN';

  private $request;
  private $response;
  private $session;
  private $tokenLength;

  public function __construct(
    Request $request,
    Response $response,
    Session $session,
    $tokenLength = 16
  ){
    $this->request = $request;
    $this->response = $response;
    $this->session = $session;
    $this->tokenLength = $tokenLength;
  }

  // @todo segregate !
  public function setAngularXsrfToken()
  {
    $xsrfToken = bin2hex(openssl_random_pseudo_bytes(16));
    $this->response->headers->setCookie(
      // @todo remove new ..
      new Cookie(
        self::TOKEN_COOKIE_NAME,
        $xsrfToken,
        0, // @todo set reasonable expire
        '/', // path
        null, // domain
        false, // secure
        false // httponly 
      )
    );
    $this->session->set(
      'XSRF-TOKEN',
      $xsrfToken
    );
  }

  // @todo segregate !
  public function verifyAngularXsrfHeader()
  {
    $headerToken = $this->request->headers->get('X-XSRF-TOKEN');
    $sessionToken = $this->session->get('XSRF-TOKEN');

    return ($headerToken === $sessionToken);
  }
}
