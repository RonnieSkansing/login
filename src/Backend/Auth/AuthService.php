<?php
namespace Skansing\Bog\Backend\Auth;

use \Skansing\Bog\Backend\User\UserService;
use \Skansing\Bog\Backend\User\EmailFactory;
use \Skansing\Bog\Backend\User\PasswordFactory;
use \Symfony\Component\HttpFoundation\Session\Session;

class AuthService
{
  private $userService;
  private $emailFactory;
  private $passwordFactory;

  public function __construct(
    UserService $userService,
    EmailFactory $emailFactory,
    PasswordFactory $passwordFactory,
    Session $session
  ){
    $this->userService = $userService;
    $this->emailFactory = $emailFactory;
    $this->passwordFactory = $passwordFactory;
    $this->session = $session;
  }

  // @todo resolve naming conflict in email
  public function loginWithEmail($email, $password)
  {
    try {
      $email = $this->emailFactory->make($email);
      $password = $this->passwordFactory->make($password);
      $hashedPassword = $this->userService->getHashedPasswordByEmail(
        $email->get()
      );
    } catch (\Exception $e) {
      // @todo handle invalid email/hash exceptions
      var_dump($e->getMessage());
      die;
    }
    $authenticated = password_verify(
      $password->get(),
      $hashedPassword
    );
    if($authenticated === false)
    {

      return false;
    }
    $this->session->migrate();
    $this->session->set(
      'loggedIn', // @todo set a hash on client and server
      true
    );

    return true;
  }

  public function logout()
  {
    $this->session->invalidate();
    $this->session->set(
      'loggedIn',
      false
    );
  }
}
