<?php
namespace Skansing\Bog\Backend\User;

class EmailFactory
{
  public function make($email)
  {
    return new Email($email);
  }
}
