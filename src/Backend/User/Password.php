<?php
namespace Skansing\Bog\Backend\User;

class Password
{
  private $password;

  public function __construct($password)
  {
    $this->validate($password);
    $this->password = $password;
  }

  private function validate($password)
  {
    if(strlen($password) > 64)
    {

      return false;
    }

    return true;
  }

  public function get()
  {
    return $this->password;
  }
}
