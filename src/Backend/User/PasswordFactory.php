<?php
namespace Skansing\Bog\Backend\User;

class PasswordFactory
{
  public function make($password)
  {
    return new Password($password);
  }
}
