<?php
namespace Skansing\Bog\Backend\User;

class Email
{
  private $email;
  private $host;
  private $id;

  public function __construct($email)
  {
    if($this->validateFormat($email) === false)
    {
      throw new \Exception('Invalid email format');
    }
    $this->email = $email;
    $this->setIdAndHost($email);
  }

  private function validateFormat($email)
  {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
  }

  public function setIdAndHost($email)
  {
    $emailSplit = explode('@', $email);
    $this->id = $email[0];
    $this->host = $email[1];
  }

  public function get()
  {
    return $this->email;
  }

  public function getHost()
  {
    return $this->host;
  }

  public function getId()
  {
    return $this->id;
  }
}
