<?php
namespace Skansing\Bog\Backend\User;

class User
{
  private $email;

  public function __construct(
    Email $email,
    PasswordHash $passwordHash = null
  ) {
    $this->email = $email;
    $this->passwordHash = $passwordHash;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function getPasswordHash()
  {
    return $this->passwordHash;
  }
}
