<?php
namespace Skansing\Bog\Backend\User;

class UserService
{
    private $userMapper;

    public function __construct(
      UserMapper $userMapper
    ) {
      $this->userMapper = $userMapper;
    }
    /**
     * Get user by email
     *
     * @param Email $email
     */
    public function getByEmail($email)
    {
      try {
        $email = $this->userMapper->getByEmail($email);

        return  $email ?: null;
      } catch(\Exception $e) {
        // @todo inject logger, log, handle
      }
    }

    public function getHashedPasswordByEmail($email)
    {
      try {
        return $this->userMapper->getHashedPasswordByEmail($email);
      } catch(\Exception $e) {
        // @todo inject logger, log, handle
      }
    }
}
