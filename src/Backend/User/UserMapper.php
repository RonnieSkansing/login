<?php
namespace Skansing\Bog\Backend\User;

use \Pdo;

class UserMapper {
  private $db;

  public function __construct(\Pdo $db)
  {
    $this->db = $db;
  }

  public function getHashedPasswordByEmail($email)
  {
    $statement = $this->db->prepare(
      'SELECT `hashedPassword` FROM `Users` WHERE `email` = :email'
    );
    $statement->bindValue(':email', $email);
    $statement->execute();
    $result = $statement->fetch(\PDO::FETCH_ASSOC);
    return $result['hashedPassword'] ?: null;
  }

  public function getByEmail($email)
  {
    $statement = $this->db->prepare(
      'SELECT `user_id`, `email` FROM `Users` WHERE `email` = :email'
    );
    $statement->bindValue(':email', $email);
    $statement->execute();
    $result = $statement->fetch(\PDO::FETCH_ASSOC);

    return $result ?: null;
  }
}
