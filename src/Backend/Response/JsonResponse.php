<?php
namespace Skansing\Bog\Backend\Response;

// @todo rename this..
class JsonResponse
{
  const SUCCESS = 'Success';
  const FAILURE = 'Failure';

  private $message;
  private $data;

  public function set($message, $data = '')
  {
    $this->message = $message;
    $this->data = $data;
  }

  public function build()
  {
    // @todo json encode failure handling
    return json_encode(
        [
          'result' => [
            'message' => $this->message,
            'data' => $this->data
          ]
        ]
    );
  }
}
