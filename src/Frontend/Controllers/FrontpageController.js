(function ()
{
  'use strict';

  angular
    .module('app')
    .controller(
      'FrontpageController',
      [ '$scope', '$rootScope', '$location', 'IdentityService', function($scope, $rootScope, $location, IdentityService)
      {
        $scope.isLoggedIn = IdentityService.isLoggedIn();
        var authStateChange = $rootScope.$on('UserAuthStateChange', function()
        {
          $scope.isLoggedIn = IdentityService.isLoggedIn();
        });
        $scope.$on('$destroy', authStateChange);
      }
    ]);
})();
