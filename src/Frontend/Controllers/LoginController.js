(function ()
{
  'use strict';

  angular
    .module('app')
    .controller('LoginController',
    [ '$scope', '$location', '$window', 'AuthenticationService', function($scope, $location, $window, AuthenticationService)
    {
      // @todo replace with focus directive or ngFocus
      var focusOnEmail = function()
      {
        $window.document.getElementById('loginEmail').focus();
      };
      $scope.html = '';
      $scope.password = '';
      focusOnEmail();

      $scope.login = function()
      {
        AuthenticationService
          .auth(
            $scope.email,
            $scope.password
          ).then(function(response) {
            // @todo replace with sane response api
            var responseData = response.data.result;
            if(responseData.message !== 'Success')
            {
              $scope.email = '';
              $scope.password = '';
              $scope.error = 'Failed to login with supplied credentials. Try again';
              focusOnEmail();

              return;
            }

            $location.url('profile');
          });
      };
    }]);
})();
