(function ()
{
  'use strict';

  angular
    .module('app')
    .controller(
      'NavigationController',
      [ '$scope', '$rootScope', '$location', 'IdentityService', function($scope, $rootScope, $location, IdentityService)
      {
        $scope.isLoggedIn = IdentityService.isLoggedIn();
        var authStateChange = $rootScope.$on('UserAuthStateChange', function()
        {
          $scope.isLoggedIn = IdentityService.isLoggedIn();
        });
        // clean up
        $scope.$on('$destroy', authStateChange);
      }
    ]);
})();
