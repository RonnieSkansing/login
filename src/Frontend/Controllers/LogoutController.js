(function ()
{
  'use strict';

  angular
    .module('app')
    .controller('LogoutController',
    [ '$scope', '$location', 'AuthenticationService', function($scope, $location, AuthenticationService)
    {
      $scope.logout = function()
      {
        AuthenticationService
          .logout().then(function(response) {
            // @todo replace with sane response api
            var responseData = response.data.result;
            if(responseData.message !== 'Success')
            {

              return;
            }

            $location.url('/');
          });
      };
    }]);
})();
