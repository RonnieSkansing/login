(function ()
{
  'use strict';

  angular
    .module('app')
    .controller('ProfileController',
    [ '$scope', 'IdentityService', function($scope, IdentityService)
    {
      $scope.email = IdentityService.getEmail();
    }
    ]);
})();
