(function()
{
  'use strict';

  angular
    .module('app')
    .factory('AuthenticationService', function($http, IdentityService)
    {
      var service = {};

      service.auth = function(email, password, callback)
      {
        // @todo frontend validation
        var promise = $http.post('/api/v0x1x0/user/login', { email: email, password: password });
        promise.then(function(response)
        {
          var responseData = response.data.result;
          if(responseData.message !== 'Success')
          {
              return;
          }
          IdentityService.loggedIn(
            responseData.data['user_id'],
            responseData.data['email']
          );
          IdentityService.save();
        });

        return promise;
      };

      service.logout = function(callback)
      {
        // @todo frontend validation
        var promise = $http.post('/api/v0x1x0/user/logout', {});
        promise.then(function(response)
        {
          // @todo dry refactor
          var responseData = response.data.result;
          if(responseData.message !== 'Success')
          {
              return;
          }
          IdentityService.loggedOut();
          IdentityService.save();
        });

        return promise;
      };

      return service;
    });

})();
