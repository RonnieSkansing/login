(function()
{
  'use strict';

  angular
    .module('app')
    .factory('IdentityService', function($rootScope, $window)
    {
      var user = {
        id: null,
        email: '',
        authenticated: false
      };
      var service = {};
      service.user = user;

      service.isLoggedIn = function()
      {
        return service.user.authenticated;
      };

      service.loggedIn = function(id, email)
      {
        service.user.id = id;
        service.user.email = email;
        service.user.authenticated = true;
        $rootScope.$emit('UserAuthStateChange');
      };

      service.loggedOut = function()
      {
        service.user.id = null;
        service.user.email = '';
        service.user.authenticated = false;
        $rootScope.$emit('UserAuthStateChange');
      };

      service.save = function()
      {
        $window.sessionStorage.user = angular.toJson(service.user);
      };

      service.getEmail = function()
      {
        return service.user.email;
      }

      service.load = function()
      {
        if($window.sessionStorage.user === undefined)
        {
          return;
        }
        service.user = angular.fromJson($window.sessionStorage.user);
      }

      return service;
    });

})();
