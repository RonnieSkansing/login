(function ()
{
  'use strict';

  function config($routeProvider, $locationProvider) {
    $routeProvider
      .when(
        '/',
        {
          controller: 'FrontpageController',
          templateUrl: 'templates/frontpage.view.html'
        })
      .when(
        '/login',
        {
          controller: 'LoginController',
          templateUrl: 'templates/login.view.html'
        })
      .when(
        '/profile',
        {
          controller: 'ProfileController',
          templateUrl: 'templates/profile.view.html'
        })
      .otherwise({ redirectTo: '/' });
  }

  config.$inject = ['$routeProvider', '$locationProvider'];
  angular
      .module('app', ['ngRoute'])
      .config(config)
      .run(function($rootScope, $location, IdentityService, $http)
      {
        IdentityService.load();
        $rootScope.$on('$locationChangeStart', function (event, next, current)
        {
          // @todo modulize this part and bundle in route config
          var path = $location.path();
          var loggedIn = IdentityService.isLoggedIn();
          if(loggedIn === false && path === '/profile')
          {
            $location.url('/login');
          }
        });
      });
})();
