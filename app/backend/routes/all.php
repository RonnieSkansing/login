<?php

$router->addGet(
  'static.frontpage',
  '/'
)->addValues([
  'controller' => \Skansing\Bog\Backend\Controllers\StaticFrontpageController::class
]);
/*
* Authentication
*
* Login / Logout
*/
$router->addPost(
  'api.authenticate.login',
  '/api/v0x1x0/user/login'
)->addValues([
  'controller' => \Skansing\Bog\Backend\Controllers\LoginController::class
]);

$router->addPost(
  'api.authenticate.logout',
  '/api/v0x1x0/user/logout'
)->addValues([
  'controller' => \Skansing\Bog\Backend\Controllers\LogoutController::class
]);
