<?php
/*
* MySQL
*/
$injector->share('PDO');
$injector->define('PDO', [
  ':dsn' => 'mysql:dbname=bog;host=127.0.0.1',
  ':username' => 'root',
  ':passwd' => 'admin'
]);
/*
* Request / Response
*/
$injector->share(\Symfony\Component\HttpFoundation\Response::class);
$injector->share(\Symfony\Component\HttpFoundation\Request::class);
$injector->delegate(\Symfony\Component\HttpFoundation\Request::class, function()
{
  return \Symfony\Component\HttpFoundation\Request::createFromGlobals();
});
/*
* Session
*/
$injector->share(Symfony\Component\HttpFoundation\Session\Session::class);
$injector->delegate(Symfony\Component\HttpFoundation\Session\Session::class, function ()
{
  $session = new Symfony\Component\HttpFoundation\Session\Session;
  $session->setName('bog');
  $session->start();

  return $session;
});
/*
* XSRF
*/
$injector->share(\Skansing\Bog\Backend\XSRF\TokenService::class);

/*
* User
*/
