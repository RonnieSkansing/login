<?php
require __DIR__.'/../../vendor/autoload.php';

/*
* Bindings
*/
$injector = new \Auryn\Injector;
$binder = function(\Auryn\Injector $injector)
{
  require __DIR__.'/bindings.php';
};
$binder($injector);

/*
* Routes
*/
$routerFiller = function(\Aura\Router\RouterFactory $routerFactory)
{
  $router = $routerFactory->newInstance();
  // fill in routes
  require __DIR__.'/routes/all.php';
  // ..
  return $router;
};
$router = $routerFiller(
  $injector->make(\Aura\Router\RouterFactory::class)
);

/*
* Route Matching
*/
$path = parse_url($_SERVER['REQUEST_URI'], \PHP_URL_PATH);
$route = $router->match($path, $_SERVER ?: []);

/*
* Essential headers
* @todo put in response object
* @todo implement middleware...
*/
header('X-Frame-Options: deny');
header('X-XSS-Protection: 1; mode=block');
header('X-Content-Type-Options: nosniff');
//header('Content-Security-Policy: default-src \'self\'');
header('X-Powered-By: ');

/*
* No matching route
* @todo
*/
if($route === false)
{
  // get the first of the best-available non-matched routes
  $failure = $router->getFailedRoute();

  // inspect the failed route
  if ($failure->failedMethod()) {
      // the route failed on the allowed HTTP methods.
      // this is a "405 Method Not Allowed" error.
  } elseif ($failure->failedAccept()) {
      // the route failed on the available content-types.
      // this is a "406 Not Acceptable" error.
  } else {
      // there was some other unknown matching problem.
  }

  var_dump($route, $path);
  header('501 Not Implemented', true, 501);
  return;
}

/*
* Dispatching
*/
try {
  $controller = $injector->make(
    $route->params['controller']
  );

// @todo } catch(\AurynCantMakeDependencyException) { }
} catch(\Exception $e) {
  var_dump($route->params['controller'], $e);
  die(__FILE__);
 // @todo a exception should not float up here!!
} finally {
  // @todo service a nice exceptional failure.
}

/*
* Run action
*/
$content = $controller->index();

/*
* Generate Angular CSRF
*/
$tokenService = $injector->make(
  \Skansing\Bog\Backend\XSRF\TokenService::class
);
$tokenService->setAngularXsrfToken();

/*
* Output
*/
$response = $injector->make(\Symfony\Component\HttpFoundation\Response::class);
$response->setContent($content);
$response->send();
